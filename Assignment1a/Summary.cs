﻿using System;
using System.Collections.Generic;

namespace Assignment1a
{
    public class Summary
    {
        public ClientDetails details;
        public Jobdescription desc;
        public Eligibilty eligible;
        public Availablity avail;

        //CONSTRUCTOR for 
        public Summary(ClientDetails c,Jobdescription d,Eligibilty e,Availablity a)
        {
            details = c;
            desc = d;
            eligible = e;
            avail = a;

        }
        //method to show summary of submitted details
        public string showSummary()
        {
            string summary = "<br><h3>Your Details Submitted are:</h3>";
            summary += "<fieldset>";
            summary += "Your DOB : " + details.ClientDOB+ " and Your are : "+ details.ClientGender;
            summary += "<br>You are looking for : " + desc.jobTitle ;
            summary += "<br>With location : " + desc.jobLocation;
            summary += "<br>You are looking for : " + desc.jobPosition + " Position";
            summary += "<br>Type of job selected: " + String.Join(", ",desc.jobtype.ToArray());
            summary += "<br>You have Experiance as: " + eligible.expYear + " Years and " + eligible.expMonth + " Months ";
            summary += "<br>You Mention " + eligible.needVisa();
            summary += "<br>Your Available for Interview On : " + string.Join(", ", avail.daysOfAvailablity.ToArray());
            summary += "<br></fieldset>";

            return summary; 
        
        }

        //method to show result ofsummary which was submitted 

        public string showResult()
        {
            List<Companies> l = eligible.CompanyInformation(eligible.expYear, eligible.expMonth,avail.daysOfAvailablity);

            string summaryResult = "<br><br><h3>Result for your search is:</h3>";
            summaryResult += "<fieldset><table border='1'>";
            summaryResult += "<tr><th>" + "Company Name" + "</th><th>" + "Designation" + "</th><th>" + "Location" +"</th><th>"+"Interview Day"+"</th></tr>";

            foreach (Companies c in l)
            {
                Companies comp = c;
                summaryResult += "<tr><td>" + c.companyName+ "</td><td>" + c.designation + "</td><td>" + c.complocation+"</td><td>" + c.interDay+"</td></tr>";

            }

            summaryResult += "<br></table></fieldset>";

            return summaryResult;
        }



    }
}
