﻿using System;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Assignment1a
{

    public partial class assb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {}




        //In this assignment i have created 5 classes:
        //ClientDetails :stores client details
        //Jobdescription:stores job details
        //Availablity: stores available days of interview
        //Companies: companies details for creating small database
        //Summary: provide summary of submited details and search result accrding to companies available

        //Review if onclick function from submitting the web form
        protected void Review(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            //enter details for client class details
            ClientDetails c = new ClientDetails();

            c.ClientGender = clientGender.SelectedItem.Value.ToString();
            c.ClientDOB = clientDOB.Text.ToString();
            c.Dob = DateTime.Parse(c.ClientDOB);
            c.ClientDOB = c.Dob.ToString("dd 'of' MMMM yyyy");//https://www.codeproject.com/Questions/990061/convert-yyyy-mm-dd-to-yyyy-mm-dd-csharp
            //end


            //enter details for job description class
            string jt = clientJobTitle.Text.ToString();
            string jl = clientloction.Text.ToString();
            string jp = clientDesignation.SelectedItem.Value.ToString();
           
            //using from Professor CODE
            List<string> typeOfJob = new List<string>();

            foreach (Control control in clientJobType.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox type = (CheckBox)control;
                    if (type.Checked)
                    {
                        typeOfJob.Add(type.Text);
                    }

                }
            }
           
            Jobdescription jobdesc =  new Jobdescription(jt,jl,jp,typeOfJob);

            //end


            //enter details for eligibilty class

            int year = int.Parse(clientExperianceYear.Text);
            int month = int.Parse(clientExperianceMonths.Text);
            string visa = clientEligiblityVisa.SelectedItem.Value.ToString();
            Eligibilty eligible = new Eligibilty(year,month,visa);

            //end



            //enter details for Availablity class
            //using from Professor CODE
            List<string> availableDay = new List<string>();

            foreach (Control control in clientDayAvailable.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox day = (CheckBox)control;
                    if (day.Checked)
                    {
                        availableDay.Add(day.Text);
                    }

                }
            }

            Availablity avail = new Availablity(availableDay);
            //end


            //enter details for summary class
            Summary show = new Summary(c, jobdesc, eligible, avail);


            //shoing summary of the submited details

            string submitedDetails = show.showSummary();
            summary.InnerHtml = submitedDetails;

            //showing  searchresult of the submited details
            summaryResult.InnerHtml = show.showResult();
        }

    }
}
