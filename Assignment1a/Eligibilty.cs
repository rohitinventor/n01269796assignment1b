﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace Assignment1a
{
    public class Eligibilty
    {
        public int expYear;
        public int expMonth;
        public string visaNeed;
        public List<string> companies;

        //adding properties using parametric constructor
        public Eligibilty(int year, int month, string visa) 
        {
            expYear = year;
            expMonth = month;
            visaNeed = visa;
        }


       

        //validating for visa requirement
        public string needVisa()
        {
            if (visaNeed.Equals("Yes"))
            { return "Need Visa"; }

            else if (visaNeed.Equals("No"))
            { return "Dont need Visa"; }
            else
            { return "Have Not selected for the Visa Field"; }
        }




        //validating for companies eligible for the client
        public List<Companies> CompanyInformation(int year, int month, List<string> day)
        {
            //https://stackoverflow.com/questions/13186046/add-item-to-list-in-a-class-in-c-sharp
            //creating small database for list of companies with  thier name,location,position and interview date
            Companies c1 = new Companies("DELL", "toronto", "HR", "Monday");
            Companies c2 = new Companies("RELIANCE", "Missisaga", "sales", "Monday");
            Companies c3 = new Companies("APPLE", "toronto", "IT", "Monday");
            Companies c4 = new Companies("HCL", "delhi", "HR", "Monday");

            Companies c5 = new Companies("CANADA POST", "toronto", "Finance", "Tuesday");
            Companies c6 = new Companies("INVENTORS", "otawa", "HR", "Tuesday");
            Companies c7 = new Companies("CANADA COMPUTERS", "toronto", "Electrical Engineering", "Tuesday");
            Companies c8 = new Companies("RELIGARE", "vancuver", "IT", "Tuesday");

            Companies c9 = new Companies("HCL", "toronto", "HR", "Wednesday");
            Companies c10 = new Companies("TCS", "paris", "ADMIN", "Wednesday");
            Companies c11 = new Companies("APPLE", "london", "HR", "Wednesday");
            Companies c12 = new Companies("SAMSUNG", "delhi", "sales", "Wednesday");


            Companies c13 = new Companies("APPLE", "london", "HR", "Thursday");
            Companies c14 = new Companies("SAMSUNG", "delhi", "sales", "Thursday");
            Companies c15 = new Companies("CANADA COMPUTERS", "toronto", "Electrical Engineering", "Thursday");
            Companies c16 = new Companies("RELIGARE", "nirmingham", "IT", "Thursday");


            Companies c17 = new Companies("TD BANK", "oshawa", "HR", "Friday");
            Companies c18 = new Companies("GOOGLE", "paris", "electrical engineering", "Friday");
            Companies c19 = new Companies("INVENTORS", "london", "it", "Friday");

            Companies c20 = new Companies("TCS", "oshawa", "HR", "Saturday");
            Companies c21 = new Companies("GOOGLE", "paris", "HR", "Saturday");
            Companies c22 = new Companies("INVENTORS", "london", "sales", "Saturday");
            Companies c23 = new Companies("CANADA POST", "toronto", "admin", "Saturday");
            Companies c24 = new Companies("SCOTIA BANK", "missisaga", "sales", "Saturday");
            List<Companies> l = new List<Companies>();

            //calculating total experiance in months
            int totalExperiance = (year * 12) + month;

            // looping for list of available days
            foreach (string d in day)
            {
                //validating for experiance and days of availablity

                if (totalExperiance <= 12 && totalExperiance >= 0 && d.Equals("Monday"))
                {
                    l.Add(c1);
                    l.Add(c2);
                    l.Add(c3);
                    l.Add(c4);
                }
                else if (totalExperiance <= 48 && totalExperiance > 12 && d.Equals("Tuesday"))
                {

                    l.Add(c5);
                    l.Add(c6);
                    l.Add(c7);
                    l.Add(c8);
                }
                else if (totalExperiance <= 48 && totalExperiance > 12 && d.Equals("Tuesday"))
                {

                    l.Add(c5);
                    l.Add(c6);
                    l.Add(c7);
                    l.Add(c8);
                }


                else if (totalExperiance <= 90 && totalExperiance > 48 && d.Equals("Wednesday"))
                {
                    l.Add(c9);
                    l.Add(c10);
                    l.Add(c11);
                    l.Add(c12);
                }
                else if (totalExperiance <= 100 && totalExperiance > 90 && d.Equals("Thursday"))
                {
                    l.Add(c13);
                    l.Add(c14);
                    l.Add(c15);
                    l.Add(c16);
                }

                else if (totalExperiance <= 110 && totalExperiance > 100 && d.Equals("Friday"))
                {
                    l.Add(c17);
                    l.Add(c18);
                    l.Add(c19);

                }

                else if (totalExperiance > 120 && d.Equals("Saturday"))
                {
                    l.Add(c20);
                    l.Add(c21);
                    l.Add(c22);
                    l.Add(c23);
                }

            }
            return l;
        }
        
    
    
    }
}
