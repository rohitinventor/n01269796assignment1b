﻿using System;
namespace Assignment1a
{
    public class ClientDetails
    {
        //elements
        private string clientGender;
        private string clientDOB;
        private DateTime dob;

        //property accessor
        public string ClientGender
        { 
            get { return clientGender; }
            set { clientGender = value; }
        }
        //a property accessor
        public string ClientDOB
        { 
            get { return clientDOB; }
            set { clientDOB = value; }
        }

        public DateTime Dob
        {
            get { return dob; }
            set { dob = value; }
        }




    }
}
