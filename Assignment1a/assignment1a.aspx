<%@ Page Language="C#" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>HUMBER JOB PORTAL</title>

</head>
<body>
    <p style="text-align:center">HUMBER JOB PORTAL</p>  

    <form id="form1" runat="server">
            <fieldset>
            <label for="title">What</label>
            <asp:Textbox runat="server" id="title" placeholder="job title, keywords"></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a title" ControlToValidate="title" ID="v1"></asp:RequiredFieldValidator>
            <br/>
            
            <label for="loc">Location</label>
            <asp:Textbox runat="server" id="loc" placeholder="Toronto, ON"></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a location" ControlToValidate="loc" ID="v2"></asp:RequiredFieldValidator>
           
            <br/>
            
            
            <label>Job type</label>    
            <asp:CheckBox runat="server" ID="part" Text="Part Time" />
            <asp:CheckBox runat="server" ID="full" Text="Full Time" />
            <asp:CheckBox runat="server" ID="contract" Text="Contract" />
            
            
            
            <br/>
            <label for="field">Position</label>
             <asp:DropDownList runat="server" ID="field">
                <asp:ListItem Value="Select" Text="--Select---"></asp:ListItem>
                <asp:ListItem Value="Electrical Engineering" Text="Electrical Engineering"></asp:ListItem>
                <asp:ListItem Value="M" Text="Accounts"></asp:ListItem>
                <asp:ListItem Value="Accounts" Text="Hr"></asp:ListItem>
                <asp:ListItem Value="Admin" Text="Admin"></asp:ListItem>
                <asp:ListItem Value="Finance" Text="Finance"></asp:ListItem>
                <asp:ListItem Value="IT" Text="IT"></asp:ListItem>
                <asp:ListItem Value="Sales" Text="Sales"></asp:ListItem>
            </asp:DropDownList>

            <br/>
            
              <label for="eligiblity">Do you need a Visa to work in Canada</label>
            <asp:RadioButton runat="server" Text="Yes" GroupName="eligiblity" Checked/>
            <asp:RadioButton runat="server" Text="No" GroupName="eligiblity"/>
               <br/>
            <label>Available For Interview</label>    
            <asp:CheckBox runat="server" ID="Mon" Text="Monday" />
            <asp:CheckBox runat="server" ID="Tue" Text="Tuesday" />
            <asp:CheckBox runat="server" ID="Wed" Text="Wednesday" />
            <asp:CheckBox runat="server" ID="Thur" Text="Thursday" />
            <asp:CheckBox runat="server" ID="Fri" Text="Friday" />
            <asp:CheckBox runat="server" ID="Sat" Text="Saturday" />
             <br/>   
            
            
            <label for="DOB">DOB</label>
            <asp:TextBox runat="server" ID="DOB" placeholder="YYYY-MM-DD"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Date of Birth" ControlToValidate="DOB" ID="v4"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="v5" runat="server" ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$" ControlToValidate="DOB" ErrorMessage="Invalid Format"></asp:RegularExpressionValidator>

            <br />
            
            <label >Your Experience:</label>
            <br/>
            <label for="expY" >Year</label>
            <asp:TextBox runat="server" ID="expY" placeholder="Years"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="expY" ID="v6"></asp:RequiredFieldValidator>
            <asp:RangeValidator EnableClientScript="true" ID="v7" runat="server" ControlToValidate="expY" MinimumValue="0" Type="Integer" MaximumValue="100" ErrorMessage="Please Enter a Value 0 100"></asp:RangeValidator>
            <br/>
            <label for="expM" >Months</label>
            <asp:TextBox runat="server" ID="expM" placeholder="Months"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="expM" ID="v8"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ID="v9" ControlToValidate="expM" Type="Integer" MinimumValue="1" MaximumValue="12" ErrorMessage="Enter the valid Months  (between 1 and 12)"></asp:RangeValidator>
            <br />
            <label for="gen">Gender</label>
            <asp:RadioButton runat="server" Text="Male" GroupName="gen" Checked/>
            <asp:RadioButton runat="server" Text="Female" GroupName="gen"/>
            
            <br />

         <asp:Button runat="server" ID="myButton"  Text="Submit"/>
         </fieldset>
          <asp:validationsummary id="v13" runat="server"  HeaderText="This is required fields"  ShowSummary="TRUE" backcolour="blue" width="340"  font-size="small"></asp:validationsummary>

    </form>
</body>
</html>
