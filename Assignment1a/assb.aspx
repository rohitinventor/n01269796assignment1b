﻿<%@ Page Language="C#" enableEventValidation="true" CodeBehind ="assb.aspx.cs" Inherits ="Assignment1a.assb" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>HUMBER JOB PORTAL</title>

</head>
<body runat="server">
        
    <p style="text-align:center">HUMBER JOB PORTAL</p> 
   
     <form id="clientDetailsForm" runat="server">
            <fieldset>
            <label for="clientJobTitle">Desired Job Title:</label>
            <asp:Textbox runat="server" id="clientJobTitle" placeholder="job title, keywords"></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a title" ControlToValidate="clientJobTitle" ID="clientJobTitleRequiredFieldValid"></asp:RequiredFieldValidator>
            <br/>
            
            <label for="clientloction">Location</label>
            <asp:Textbox runat="server" id="clientloction" placeholder="Toronto, ON"></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a location" ControlToValidate="clientloction" ID="clientloctionRequiredFieldValid"></asp:RequiredFieldValidator>
           
            <br/>
            <label>Job type</label>   
            <div id="clientJobType" runat="server">
            <asp:CheckBox runat="server" ID="part" Text="Part Time" />
            <asp:CheckBox runat="server" ID="full" Text="Full Time" />
            <asp:CheckBox runat="server" ID="contract" Text="Contract" />
            </div>
            
            
            <br/>
            <label for="clientDesignation">Position</label>
             <asp:DropDownList runat="server" ID="clientDesignation">
                <asp:ListItem Value="Electrical Engineering" Text="Electrical Engineering"></asp:ListItem>
                <asp:ListItem Value="Accounts" Text="Accounts"></asp:ListItem>
                <asp:ListItem Value="Hr" Text="Hr"></asp:ListItem>
                <asp:ListItem Value="Admin" Text="Admin"></asp:ListItem>
                <asp:ListItem Value="Finance" Text="Finance"></asp:ListItem>
                <asp:ListItem Value="IT" Text="IT"></asp:ListItem>
                <asp:ListItem Value="Sales" Text="Sales"></asp:ListItem>
            </asp:DropDownList>

            <br/>
            
            <label for="clientEligiblityVisa">Do you need a Visa to work in Canada</label>
            <asp:RadioButtonList runat="server" ID="clientEligiblityVisa">
              <asp:ListItem Text="Yes" >Yes</asp:ListItem>
              <asp:ListItem Text="No" >No</asp:ListItem>
            </asp:RadioButtonList>       
        
            <br/>
              
            <div id="clientDayAvailable" runat="server">
            <label>Available For Interview</label>    
            <asp:CheckBox runat="server" ID="Mon" Text="Monday" />
            <asp:CheckBox runat="server" ID="Tue" Text="Tuesday" />
            <asp:CheckBox runat="server" ID="Wed" Text="Wednesday" />
            <asp:CheckBox runat="server" ID="Thur" Text="Thursday" />
            <asp:CheckBox runat="server" ID="Fri" Text="Friday" />
            <asp:CheckBox runat="server" ID="Sat" Text="Saturday" />
            </div>
             <br/>   
            
            
            <label for="clientDOB">DOB</label>
            <asp:TextBox runat="server" ID="clientDOB" placeholder="YYYY-MM-DD"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Date of Birth" ControlToValidate="clientDOB" ID="clientDOBRequiredFieldValid"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="dobRegularExpressionValidator" runat="server" ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$" ControlToValidate="clientDOB" ErrorMessage="Invalid Format"></asp:RegularExpressionValidator>

            <br />
            
            <label >Your Experience:</label>
            <br/>
            <label for="clientExperianceYear" >Year</label>
            <asp:TextBox runat="server" ID="clientExperianceYear" placeholder="Years"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="clientExperianceYear" ID="clientExperianceYearRequiredFieldValid"></asp:RequiredFieldValidator>
            <asp:RangeValidator EnableClientScript="true" ID="clientExperianceYearRangeValidator" runat="server" ControlToValidate="clientExperianceYear" MinimumValue="0" Type="Integer" MaximumValue="100" ErrorMessage="Please Enter a Value 0 100"></asp:RangeValidator>
            <br/>
            <label for="clientExperianceMonths" >Months</label>
            <asp:TextBox runat="server" ID="clientExperianceMonths" placeholder="Months"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="clientExperianceMonths" ID="clientExperianceMonthsRequiredFieldValid"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ID="clientExperianceMonthsRangeValid" ControlToValidate="clientExperianceMonths" Type="Integer" MinimumValue="1" MaximumValue="12" ErrorMessage="Enter the valid Months  (between 1 and 12)"></asp:RangeValidator>
            <br />
                
             <asp:RadioButtonList runat="server" ID="clientGender">
              <asp:ListItem Text="Male" >Male</asp:ListItem>
              <asp:ListItem Text="Female" >Female</asp:ListItem>
            </asp:RadioButtonList>       
          
           
            <br />

         <asp:Button runat="server" ID="myButton"  OnClick="Review" Text="Submit"/>
         </fieldset>
          <asp:validationsummary id="validationsummary" runat="server"  HeaderText="This is required fields"  ShowSummary="TRUE" backcolour="blue" width="340"  font-size="small"></asp:validationsummary>

    </form>
       
        
     <div id="summary" runat="server"></div>
        
        <div id="summaryResult" runat="server"></div>
</body>
</html>
